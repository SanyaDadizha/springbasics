package basic1.logger;

import java.util.ArrayList;
import java.util.List;
import basic1.beans.Event;

public class CacheFileEventLogger extends FileEventLogger {
    private int cacheSize;
    private List<Event> cache;


    public CacheFileEventLogger (int cacheSize, String filename){
        super(filename);
        this.cacheSize = cacheSize;
        this.cache = new ArrayList<>(cacheSize);
    }
    public void destroy(){
        if (!cache.isEmpty()){
            writeEventsFromCashe();
            System.out.println("writeD");
        }
        System.out.println("destroy");
    }
    @Override
    public void logEvent(Event event){
        cache.add(event);

       if(cache.size() == cacheSize){
           writeEventsFromCashe();
           cache.clear();
           System.out.println("cash clear");
       }
    }

    public void writeEventsFromCashe(){
         cache.stream().forEach(super::logEvent);
        System.out.println("writeEvents");
        //cache.forEach(super::logEvent);
    }
}
