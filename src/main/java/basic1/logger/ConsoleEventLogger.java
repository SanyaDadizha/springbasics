package basic1.logger;

import basic1.beans.Event;

public class ConsoleEventLogger implements EventLogger {


    @Override
    public void logEvent(Event event){
        System.out.println(event.toString());
    }
}
