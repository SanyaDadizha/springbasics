package basic1.logger;

import basic1.beans.Event;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class FileEventLogger implements EventLogger {

    private static File file;
    /*static*/ String filename;

    public FileEventLogger(String filename) {
        this.filename=filename;
    }

    public void init(){
        file = new File(filename);
        if (file.exists() && !file.canWrite()){
            throw new IllegalArgumentException("Can'twrite to file " + filename);
        }else if (!file.exists()){
            try {
                file.createNewFile();
            }catch (Exception e){
                throw new IllegalArgumentException("Can't create file", e);
            }
        }

    }
    @Override
    public void logEvent(Event event) {

        try {
            FileUtils.writeStringToFile(file, event.toString(),true);
            System.out.println("writeToFile");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
