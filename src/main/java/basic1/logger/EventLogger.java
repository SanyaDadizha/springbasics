package basic1.logger;

import basic1.beans.Event;

public interface EventLogger {
     void logEvent(Event event);
}
