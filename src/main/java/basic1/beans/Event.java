package basic1.beans;

import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class Event {

    private static final AtomicInteger AUTO_ID = new AtomicInteger();

    String msg;
    Date date;
    int id;

    DateFormat df;



    public Event(Date date, DateFormat df) {
        this.date = date;
        this.df = df;
        this.id = AUTO_ID.getAndIncrement();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return
        "Event [id=" + id + ", msg=" + msg + ", date=" + df.format(date) + "]";
    }

}
