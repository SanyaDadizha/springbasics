package basic1;

import basic1.beans.Client;
import basic1.beans.Event;
import basic1.logger.ConsoleEventLogger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    Client client;
    ConsoleEventLogger eventLogger;

    public App(Client client, ConsoleEventLogger eventLogger) {
        super();
        this.client = client;
        this.eventLogger = eventLogger;
    }

    public static void main(String[] args) {


        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppContext.class);
        App app = ctx.getBean(App.class);

        Event event = ctx.getBean(Event.class);

        app.logEvent(event,"Some event for 1");



        event = ctx.getBean(Event.class);

        app.logEvent(event,"Some event for 2");
        event = ctx.getBean(Event.class);
        app.logEvent(event,"Some event for 3");
        app.logEvent(event,"Some event for 4");

        app.logEvent(event,"Some event for user 1");

         ((AnnotationConfigApplicationContext) ctx).close();

      /*  List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        numbers.forEach(integer -> System.out.println(integer));

        numbers.stream()
                .map(integer -> integer*2)
                .forEach(System.out::println);*/
    }


       // App app = new App(Client c);

       // app.client = new Client("1","SD");
      //  app.eventLogger = new ConsoleEventLogger();


    private void logEvent(Event event, String msg){
        String message = msg.replaceAll(client.getId(),client.getFullName());
        event.setMsg(message);
        eventLogger.logEvent(event);

    }

}
