package basic1;

import basic1.beans.Client;
import basic1.beans.Event;
import basic1.logger.CacheFileEventLogger;
import basic1.logger.ConsoleEventLogger;

import basic1.logger.FileEventLogger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.text.DateFormat;
import java.util.Date;

@Configuration
public class AppContext {

    @Bean
    public Client client () {
        return new Client("2","SS");}
    @Bean
    public ConsoleEventLogger eventLogger(){
        return new ConsoleEventLogger();
    }
    @Bean
    public App app (){
        return new App(client(),eventLogger());
    }
    @Bean
    @Scope ("prototype")
    public Event event(){
        return new Event(date(),df());
    }
    @Bean
    @Scope("prototype")
    public Date date(){
        return new Date();
    }
    @Bean
    public DateFormat df(){
       return DateFormat.getDateTimeInstance();
    }

    @Bean
    public FileEventLogger fileEventLogger(){return new FileEventLogger("target/events_log.txt");}
   // fileEventLogger().init();}
    @PostConstruct
    public void init(){
        fileEventLogger().init();
    }
    @Bean
    public CacheFileEventLogger cacheFileEventLogger() {return new CacheFileEventLogger(2,"target/events_log.txt");}
    @PreDestroy
    public void destroy(){cacheFileEventLogger().destroy();}


    

}
